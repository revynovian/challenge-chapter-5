class Game {
  static CHOICES = [{
    "choice": "rock",
    "beats": "scissor",
    "losesTo": "paper"
    },{
    "choice": "paper",
    "beats": "rock",
    "losesTo": "scissor"
    },{
    "choice": "scissor",
    "beats": "paper",
    "losesTo": "rock"
    }];

  constructor(player,cpuPlayer){
    this.playerChoice = player;
    this.cpuChoice = cpuPlayer;
    this.gameturn = 0;
    this.el = 0;    
    //dom selector
    this.allhand = document.getElementsByClassName('player');
    this.cpuallhand = document.getElementsByClassName('cpu');
    this.result = document.getElementById('result');
    this.pscoreDisp = document.getElementById('p-score');
    this.cscoreDisp= document.getElementById('c-score');
    this.resetBt = document.getElementById('reset-button');
  }
  //main game method
  startGame () {
    const result = this.compare();
    this.showResult(result);
    this.showSelected();
    this.displayScore(this.stats(result));
    this.resetButton();
  }  
  //find a winner
  compare() {
    for (this.gameturn in Game.CHOICES) {
      if (this.cpuChoice.getChoice == Game.CHOICES[this.gameturn].choice) {
        if (Game.CHOICES[this.gameturn].losesTo.includes(this.playerChoice.getChoice)) {
          return 'YOU WIN';
        }
        else if (Game.CHOICES[this.gameturn].beats.includes(this.playerChoice.getChoice)) {
          return 'YOU LOSE';
        }
        else return 'DRAW';
      }
    }
  }
  //selected css
  showSelected() {
    this.clicked = document.getElementById(this.playerChoice.getChoice);
    //remove selected player and cpu
    for (this.el of this.allhand){
      this.el.classList.remove('selected','animation');
    }
    for (this.el of this.cpuallhand){
      this.el.classList.remove('selected-cpu','animation');
    }
    //show selected player and cpu
    this.clicked.classList.add('selected','animation');
    this.cpuallhand[`${this.cpuChoice.getChoice}-cpu`].classList.add('selected-cpu','animation')
  }
  showResult (msg) {
    this.result.innerHTML = `${msg} <br>`;
    // adding background class to result message
    if (msg == 'YOU WIN') {
      this.result.classList.remove('bg-default','bg-lose','bg-draw');
      this.result.classList.add('bg-win');
    }
    else if (msg == 'YOU LOSE') {
      this.result.classList.remove('bg-default','bg-win','bg-draw');
      this.result.classList.add('bg-lose');
    }
    else if (msg == "DRAW") {
      this.result.classList.remove('bg-lose','bg-win','bg-default',);
      this.result.classList.add('bg-draw');
    }
    else {
      this.result.classList.remove('bg-lose','bg-win','bg-draw');
      this.result.classList.add('bg-default');
    }
  }    
  //statistic or score counter
  stats (status) {    
    if (status == 'YOU WIN') Main.pcounter++;
    else if (status == 'YOU LOSE')Main.ccounter++;
    return [Main.pcounter,Main.ccounter];
  }
  //render score to html
  displayScore(score) {
    this.pscoreDisp.innerHTML = `${score[0]}`;
    this.cscoreDisp.innerHTML = `${score[1]}`;
  }
  //reset score and remove class button
  resetButton () {
    this.resetBt.addEventListener('click', function() {
      Main.pcounter = 0;
      Main.ccounter = 0;
      const allhand = document.getElementsByClassName('player');
      const cpuallhand = document.getElementsByClassName('cpu');
      const score = [Main.pcounter,Main.ccounter]
      const game = new Game();
      game.displayScore(score);
      game.showResult('VS');
      for (this.el of allhand){
        this.el.classList.remove('selected','animation');
      }
      for (this.el of cpuallhand){
        this.el.classList.remove('selected-cpu','animation');
      }
    })
  }
}

class Players {
  constructor() {
    this.getChoice = this.getChoice();
  }
  getChoice() {
    return this.getChoice;
  }
}

class Humanplayer extends Players {
  constructor() {
    super();
  }
  getChoices(playerClick){
    this.getChoice = playerClick;
  }
}
class CpuPlayer extends Players {
  constructor(){
    super();
  }
  getChoice() {
    let cpuHand = Game.CHOICES[Math.floor(Math.random() * Game.CHOICES.length)].choice;
    return cpuHand;
  }
}

class Main {
  static pcounter = 0;
  static ccounter = 0;
  constructor(){
    this.playerElem = [...document.getElementsByClassName('player')];
  }
  Play() {
  // when player click image
    this.playerElem.forEach(function(e){ 
      e.addEventListener('click', function() {
        // instantiate object from classes
        const player1 = new Humanplayer();
        const cpu1 = new CpuPlayer();
        const newGame1 = new Game(player1,cpu1);
        player1.getChoices(e.id);
        newGame1.startGame();
      })
    });
  }
}

const newGame = new Main();
newGame.Play();