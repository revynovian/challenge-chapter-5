// 1. load data from JSON
const { loadFile , saveFile } = require("../data/load-data")
const file = loadFile();
const isValid = require('../utils/validator');

// 2. request handler
// 2.1 retrieve all user's data
function getUserall (req,res){
  res.status(200)
  if(file.length == 0) res.json({message: "data is empty"});
  else res.json(file);
}
// 2.2 get user using id on path-variable(params)
function getUser (req,res){
  const getData = file.find(e => e.id == req.params.id)
  if (!getData) return res.status(404).json({status:'fail', message: `No such entity with id : ${req.params.id}`});  
  res.status(200).json({ status: 'success',data : getData});
}
// 2.3 create new user using request body
function createUser (req,res){
  const newData = {
    id: file.length+1,
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  }
  // validation
  const { notValid, message }= isValid(req.body);
  if(notValid) {
    res.status(400).send({status: 'fail',message})
  }
  else {
    file.push(newData);
    saveFile(file);
    res.json({status: 'success',data: newData});
  }
}
// 2.4 update user's data using id and request body
function updateUser (req,res) {const getData = file.find((e) => e.id == req.params.id);
  if (!getData) return res.status(404).json({status:'fail', message: `No such entity with id : ${req.params.id}`});
  const index = file.findIndex(e => e.id == req.params.id)
  
  // validation
  const { notValid, message }= isValid(req.body);
  if(notValid) {
    res.status(400).send({status: 'fail',message})
  }
  else {
  const {name, email, password} = req.body
  file[index] = {
    id: parseInt(req.params.id),
    name,
    email,
    password
  }
  saveFile(file);
  res.status(200).json({ status: 'success',data : file[index]});
  }
}
// 2.5 delete user's data using id
function deleteUser (req,res) {
  const getData = file.find((e) => e.id == req.params.id);
  if (!getData) return res.status(404).json({status:'fail', message: `No such entity with id : ${req.params.id}`});

  const index = file.findIndex(e => e.id == req.params.id)
  file.splice(index,1);
  saveFile(file);
  res.status(200).json({ status: 'success'});
}

module.exports = {getUserall, getUser, createUser, updateUser, deleteUser}