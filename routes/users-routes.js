const express = require("express");
const router = express.Router();
const users = require('../controller/users-controller')

//api routes
router.get('/', users.getUserall);
router.get('/:id', users.getUser);
router.post('/', users.createUser);
router.put('/:id', users.updateUser);
router.delete('/:id', users.deleteUser)

module.exports = router;