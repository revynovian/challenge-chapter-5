const fs = require('fs');

const loadFile = () => {
  const file = fs.readFileSync('data/users.json')
  const data = JSON.parse(file)
  return data;
}
const saveFile = (data)=>{
  fs.writeFileSync('data/users.json',JSON.stringify(data))
}

module.exports = { loadFile , saveFile }