# Chapter 5 - Challenge

## Express Web Server and REST API


|    Endpoint      |  Description    |
| :--------------  | :------------------- |
|        /         |  Landing Page        |
|      /game       |  Game                |
|  /api/users      |  users API           |

--- 

### Users CRUD API

> [Download Postman Collection](https://www.getpostman.com/collections/87f399f0c49ae58d8e1b)

### List of users ###

> GET `/api/users`

Returns a list of user's data.

### Get a single user ###

> GET `/api/users/:id`

Retrieve detailed information about spesific users using id number in path variable/params.

### Create new user ###

> POST `/api/users`

The request body needs to be in JSON format and include the following properties:

 - `name` - Required - at least 3 characters
 - `email` - Required 
 - `password` - Required - atleast 6 characters

Example
```
POST /api/users
{
  "name": "Felicita_Becker",
  "email": "felicp@outlook.com",
  "password": "17F4d8lXsh2FT3A"
}
```

### Update user's data ###

> PUT `/api/usrs/:id`

Update an existing user.

Needs to specify user's id in path variable and
The request body in JSON format

 Example
```
PUT /api/users/4

{
  "name": "sabrina",
  "email": "sabrina@gmail.com",
  "password": "inipassword"
}
```

### Delete user ###

> DELETE `/api/users/:id`

Delete an existing user.

Needs to specify user's id in path variable,
and the request body needs to be empty.

 Example
```
DELETE /api/users/10
```