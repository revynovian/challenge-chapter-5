module.exports = function isValid (sample) {
  const pesan = {
    notValid: null,
    message: []
  }
  // simple validation for user's request
  if (!sample.name || sample.name.length < 3) {
    pesan.notValid = true;
    pesan.message.push('name shouldnt be empty or at least 3 characters');
  }  
  if(!sample.email || !sample.email.includes('@')) {
    pesan.notValid = true;
    pesan.message.push('invalid email');
  }
  if (!sample.password || sample.password.length < 6) {
    pesan.notValid = true;
    pesan.message.push('password shouldnt be empty or at least 6 characters');
  }
  return pesan;  
}