const express = require("express");
const app = express();
const PORT = 3000;
const router = require("./routes/users-routes");
const morgan = require("morgan");

// middleware
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.json());
app.use(morgan("tiny"));
// api routes
app.use("/api/users", router);

// main routes
app.get("/", (req, res) => {
  res.render("index", { title: "chapter-3" });
});
app.get("/game", (req, res) => {
  res.render("game", { title: "chapter-4" });
});

// server error handler 5xx
app.use((err, req, res, next) => {
  res.status(500).render("errors", { title: "error page", status: "500" });
  next();
});
// client error handler 4xx
app.use((req, res, next) => {
  res.status(404).render("errors", { title: "error page", status: "404" });
});

app.listen(PORT, () => {
  console.log(`server listening at http://localhost:${PORT}`);
});